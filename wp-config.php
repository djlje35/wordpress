<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'wrp_yaku');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'L7Eol_NwA4`oB}d7?/0W{tAkLjcNd8,W&?h/e`Texm}/S{W[CtmpXkAT>_5@IanB');
define('SECURE_AUTH_KEY', '<RPc:mYTB+Fl[1Jk;ofr}AK6;<|b-8bT,Okl0-jwDr8hw}x]y(b=>uSt*#1*HKzL');
define('LOGGED_IN_KEY', 'AZ0lyMVCGN,~kKtMo)P5FxrN}WH !lWe.ean|+|y^J84jgxRx~^O1?3<azO;{U,Y');
define('NONCE_KEY', 'q{-&;O[;KFK?`9!.<X.0XJ;kksn{MH5A;6BKdH2F{MGY#OT/E0ZUDJD%)N%uyXA2');
define('AUTH_SALT', 'hQmt{8pd_Z6bUj8S3EpU2g<,ML|Bp{!0mD_V{P-5?apnuk%0u:MX]-Kwv0o(:@Kw');
define('SECURE_AUTH_SALT', 'NjrUs]tHZ63qq,z`r-~|&+wjI0v5rIN^6?`$s[]thwqdMZ P25*zupIr$~3-F`wx');
define('LOGGED_IN_SALT', 'GNEhgdht~+?9B%mX`=9UU?D,MLXd%F*?;Yy:M#*EbEp@nis%Oe)|M0m1<In|,Jx4');
define('NONCE_SALT', 'a6#rx^FzV|2`j,:%lzJ-9^=?>3|}`V5lcJh>x/<W$k=z|c]:iIwwznUaJFiB-I2-');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

